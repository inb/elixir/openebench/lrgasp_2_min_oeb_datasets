# Lrgasp 2 Min Oeb Datasets

## Usage

```bash
# Transcoding the dataset in the minimal schema
python lrgasp_2_min_oeb_datasets.py -i mouse.csv -o mouse.json 
```

## Validation of generated content

```bash
# In order to get the schema and tools to use for the validation
git clone https://github.com/inab/OEB_level2_data_migration.git

python3 -mvenv .py3env
source .py3env/bin/activate
pip install --upgrade pip wheel
pip install extended-json-schema-validator

# Now, the validation as such
ext-json-validate --guess-schema --report val_report_mouse.json --iter-arrays OEB_level2_data_migration/oeb_level2/schemas/minimal_bdm_oeb_level2.yaml mouse.json
```