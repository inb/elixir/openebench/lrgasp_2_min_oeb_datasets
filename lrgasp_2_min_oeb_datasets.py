#!/usr/bin/env python3

import argparse
import csv
import datetime
import json
import os
import os.path
import time

def lrgasp_transcoder(input_filename, output_filename, community_acronym="LRGASP", metric_label="length"):
	print(f"Reading {input_filename}")
	with open(input_filename, mode="r", encoding="utf-8") as iF:
		iF_stat = os.stat(input_filename)
		tabR = csv.reader(iF)
		
		challenge_label_prefix = os.path.basename(input_filename)
		# Removing extension from the prefix
		c_i_dot = challenge_label_prefix.rfind(".")
		if c_i_dot > 0:
			challenge_label_prefix = challenge_label_prefix[0:c_i_dot]
		
		data_category_i = None
		tool_i = None
		data_i = None
		
		challenges = {}
		participants = {}
		
		for row in tabR:
			if data_category_i is not None:
				participant_label = row[tool_i]
				challenge_label = challenge_label_prefix + "_" + row[data_category_i]
				data_s = row[data_i]
				data = float(data_s) if '.' in data_s else int(data_s)
				challenges.setdefault(challenge_label, {}).setdefault(participant_label, []).append(data)
				participants.setdefault(participant_label, set()).add(challenge_label)
			else:
				header = dict(map(lambda t: (t[1], t[0]), enumerate(row)))
				data_category_i = header["Data_Category"]
				tool_i = header["Tool"]
				data_i = header["length"]
	
	print(f"Writing {output_filename}")
	mod_date = datetime.datetime.fromtimestamp(iF_stat.st_mtime, tz=datetime.timezone.utc).astimezone().isoformat()
	sep = '['
	with open(output_filename , mode="w", encoding="utf-8") as oH:
		# First, the participant datasets
		for participant_label, cha_set in participants.items():
			p_d_base = {
				"community_id": community_acronym,
				"datalink": {
					"attrs": [
						"archive"
					],
					"status": "ok",
					"validation_date": mod_date,
				},
				"participant_id": participant_label,
				"type": "participant"
			}
			for challenge_label in cha_set:
				p_d ={
					"_id": f"{community_acronym}:{challenge_label}_{participant_label}_P",
					"challenge_id": [
						challenge_label
					]
				}
				p_d.update(p_d_base)
				print(sep,file=oH)
				# for next iterations
				sep = ','
				prev = oH.tell()
				json.dump(p_d, oH, indent=4, sort_keys=True)
				after = oH.tell()
				print(f"(wrote JSON entry {p_d['_id']} of {after-prev} bytes)")
		# Then, the assessment datasets, the challenge reporting ones, and the aggregation datasets
		for challenge_label, challenge in challenges.items():
			a_d_base = {
				"challenge_id": challenge_label,
				"community_id": community_acronym,
				"type": "assessment",
			}
			for participant_label, data_series in challenge.items():
				a_d = {
					"_id": f"{community_acronym}:{challenge_label}_{metric_label}_{participant_label}_A",
					"metrics": {
						"metric_id": metric_label,
						"values": data_series,
					},
					"participant_id": participant_label,
				}
				
				a_d.update(a_d_base)
				print(sep,file=oH)
				# for next iterations
				sep = ','
				prev = oH.tell()
				json.dump(a_d, oH, indent=4, sort_keys=True)
				after = oH.tell()
				print(f"(wrote JSON entry {a_d['_id']} of {after-prev} bytes)")
			
			# Now the challenge reporting ones
			ch_report = {
				"id": challenge_label,
				"participants": list(challenge.keys())
			}
			print(sep,file=oH)
			# for next iterations
			sep = ','
			prev = oH.tell()
			json.dump(ch_report, oH, indent=4, sort_keys=True)
			after = oH.tell()
			print(f"(wrote JSON entry {challenge_label} of {after-prev} bytes)")
			
			
			agg_d = {
				"_id": f"{community_acronym}:{challenge_label}_{metric_label}_agg",
				"community_id": community_acronym,
				"challenge_ids": [
					challenge_label,
				],
				"datalink": {
					"inline_data": {
						"series_type": "aggregation-data-series",
						"challenge_participants": [
							{
								"label": participant_label,
								"metric_id": metric_label,
								"values": data_series,
							}
							for participant_label, data_series in challenge.items()
						],
						"labels": [
							{
								"label": participant_label,
								"dataset_orig_id": f"{community_acronym}:{challenge_label}_{participant_label}_P"
							}
							for participant_label in challenge.keys()
						],
						"visualization": {
							"type": "box-plot",
							"available_metrics": [
								metric_label
							]
						},
						
					},
				},
				"type": "aggregation"
			}
			print(sep,file=oH)
			# for next iterations
			sep = ','
			prev = oH.tell()
			json.dump(agg_d, oH, indent=4, sort_keys=True)
			after = oH.tell()
			print(f"(wrote JSON entry {agg_d['_id']} of {after-prev} bytes)")
				
		print("]", file=oH)
				

def main():
	ap = argparse.ArgumentParser(
		description="Prototype LRGASP -> OEB minimal datasets transcoder",
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
	)
	
	ap.add_argument(
		"-i",
		dest="input_filename",
		required=True,
		help="Input CSV filename from LRGASP"
	)
	
	ap.add_argument(
		"-o",
		dest="output_filename",
		required=True,
		help="Output filename whose contents follows the minimal dataset schemas"
	)
	
	args = ap.parse_args()
	
	lrgasp_transcoder(args.input_filename, args.output_filename)
	

if __name__ == '__main__':
	main()